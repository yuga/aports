# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=headscale
pkgver=0.18.0
pkgrel=0
pkgdesc="An open source, self-hosted implementation of the Tailscale control server"
url="https://github.com/juanfont/headscale"
arch="all !ppc64le !riscv64" # go build fails
license="BSD-3-Clause"
pkgusers="headscale"
pkggroups="headscale"
makedepends="go"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="https://github.com/juanfont/headscale/archive/v$pkgver/headscale-$pkgver.tar.gz
	headscale.initd
	"
options="net" # fetch dependencies

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	# move socket to a subdirectory to allow running as non-root
	sed -i 's|/var/run/headscale.sock|/var/run/headscale/headscale.sock|' config-example.yaml
}

build() {
	go build -ldflags "-X github.com/juanfont/headscale/cmd/headscale/cli.Version=v$pkgver" ./cmd/headscale
}

check() {
	make test
}

package() {
	install -Dm755 headscale "$pkgdir"/usr/bin/headscale

	install -Dm755 "$srcdir"/headscale.initd "$pkgdir"/etc/init.d/headscale
	install -Dm644 config-example.yaml "$pkgdir"/etc/headscale/config.yaml
}

sha512sums="
bc0f00e4fe9917889bc1ad59900f96f58da58ddd739d4f8d19149aa37ee6a2979dbbf6e2228a0839e018544f867db3e93aed7ae006e40b9d0c63a7814f948fba  headscale-0.18.0.tar.gz
0800829bfc087af283afc117406324a0129b30b587c8cc5df85e147ac09fc879d726fc2d0b62ed545fb0190ed887641f07256745da9dea56932dd2d90aa41625  headscale.initd
"
